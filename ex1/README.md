# Lab 1 : Modeling of human crowds

This repo contains our code for the different simulations of the first session.

## Requirements & Installation 

Requirements:
- python 3.7
- numpy
- pygame

Installation:
- Use the package manager [pip](https://pip.pypa.io/en/stable/) to install pygame.

```bash
pip install pygame
```

## Run the simulation

- clone the repo
- run main.py and change scenarios path to get the simulation corresponding to each task:

```python
# === SIMULATION ZONE ===
# Define the path to scenario that should be loaded
scenario_path = 'scenarios/task3'
```

## Implementation

- utils.py -> Contains all the python functions to compute the simulations.
- main.py -> Contains the code that will handle the graphical interface.
- scenarios repository -> Contains the different scenario for each task.

