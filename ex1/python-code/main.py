"""
 Example program to show using an array to back a grid on-screen.

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/mdTeqiWyFnc
"""
import pygame
from utils import *

# Define some colors
BLACK = (0, 0, 0)
GREY = (120, 120, 120)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

# === SIMULATION ZONE ===
# Define the path to scenario that should be loaded
scenario_path = "scenarios/task4_chicken"
# Define the algorithm you want to use to determine which path the pedestrians are taking
#Can be either "Euclidean", "cost_function", "Dijkstra"
algorithm = "cost_function"

# Load scenario
pedestrians, target_pos, grid, obstacles = init_simulation(scenario_path)
distances = compute_distances_with_dijkstra(grid)

# Initialize pygame
pygame.init()

# Cosmetic settings for drawing the GUI
MARGIN_RATIO = 0.1  # How much of the cell should be drawn as a border

# Calculate how big the cells can be drawn for a 1000x1000 window
max_cell_size = min((800.0 / len(grid[0])), (800.0 / len(grid)))
cell_size = math.floor(max_cell_size * (1 - MARGIN_RATIO))
margin_size = math.floor(max_cell_size * MARGIN_RATIO)

# Calculate the actual window size that can differ from 1000x1000 because of rounding errors
WINDOW_SIZE = [int((cell_size + margin_size) * len(grid[0]) + margin_size),
               int((cell_size + margin_size) * len(grid)) + margin_size]
screen = pygame.display.set_mode(WINDOW_SIZE)

# Set title of screen
pygame.display.set_caption("Pedestrian simulation")

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# Simulation variables
n = 0
cycles, run_forever, simulation_speed = load_simulation_variables(scenario_path)

# Loop until the user clicks the close button.
done = False

# -------- Main Program Loop -----------
while not done:
    # Check if the user wants to close the window
    for event in pygame.event.get():  # User did something
        if event.type == pygame.QUIT:  # If user clicked close
            done = True  # Flag that we are done so we exit this loop

    # Only run a certain amount of cycles or run_forever
    if n < cycles or run_forever:
        print(' --- Step {} --- '.format(n+1))
        if algorithm == "Euclidean" :
            grid, pedestrians = compute_step(grid, pedestrians, target_pos)
        if algorithm == "Dijkstra" :
            grid, pedestrians = compute_step_Dijkstra(grid, pedestrians, target_pos, distances)
        if algorithm == "cost_function" :
            grid, pedestrians = compute_step_cost_function(grid, pedestrians, target_pos, obstacles)
        n += 1

    # Set the screen background
    screen.fill(BLACK)

    # Draw the grid
    for row in range(grid.shape[0]):
        for column in range(grid.shape[1]):
            color = WHITE
            if grid[row][column] == 1:
                color = GREEN
            elif grid[row][column] == 2:
                color = RED
            elif grid[row][column] == 5:
                color = GREY
            pygame.draw.rect(screen,
                             color,
                             [(margin_size + cell_size) * column + margin_size,
                              (margin_size + cell_size) * row + margin_size,
                              cell_size,
                              cell_size])

    # Limit to simulation_speed frames per second
    clock.tick(simulation_speed)

    # Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

# Be IDLE friendly. If you forget this line, the program will 'hang'
# on exit.
pygame.quit()
