import math
import numpy as np
import configparser


def add_pedestrian(grid, position):
    grid[position[0], position[1]] = '1'
    return grid


def add_obstacle(grid, position):
    grid[position[0], position[1]] = '5'
    return grid


def add_target(grid, position):
    grid[position[0], position[1]] = '2'
    return grid


def define_grid(length, width):
    grid = np.zeros((length, width))
    return grid


def get_neighbors(grid, pedestrian):
    x, y = pedestrian[0], pedestrian[1]  # Shorthand for readability
    neighbors = []
    # Add neighbors if they are in bounds
    for i in range(x - 1, x + 2):
        for j in range(y - 1, y + 2):
            if 0 <= i < len(grid) and 0 <= j < len(grid[0]) and grid[i, j] != 5:
                neighbors.append((grid[i, j], i, j))

    return neighbors


def distance_euclid(a, b):
    return math.sqrt((b[0] - a[0]) ** 2 + (b[1] - a[1]) ** 2)


def find_next_move_euclid(pedestrian, neighbors, target_pos):
    next_move = pedestrian[:2] + (0,)
    min_distance = distance_euclid(pedestrian, target_pos)

    # Check distances of all neighbors and return the position with lowest distance to target
    for neighbor in neighbors:
        if neighbor[0] == 0:  # test to check if the neighbor is an empty cell
            distance = distance_euclid(neighbor[1:], target_pos)

            if distance < min_distance:
                min_distance = distance
                next_move = neighbor[1:] + (distance_euclid(pedestrian, neighbor[1:]),)
    # print('next_move : {}'.format(next_move))
    return next_move

def compute_cost_function(cell_index, target, obstacles) : #Compute the cost function for each cell to handle obstacles
    
    d_max = 10; #lower distance to pass the chicken test with this implementation.
    cost = distance_euclid(cell_index,target)
    
    for ob in obstacles :
        dist_ob = distance_euclid(cell_index,ob)
        if dist_ob < d_max :
            cost+=np.exp(1/dist_ob)
     
    return cost

def compute_step_cost_function(grid, pedestrians, target_position, obstacles) :
    new_positions = []
    for ped in pedestrians:
        if ped[2] <= 0:  # If pedestrian can move
            neighbors = get_neighbors(grid, ped)

            # If there's a target as neighbor, stop moving
            if 2 in [neighbor[0] for neighbor in neighbors]:
                print('Pedestrian reached the target')
                new_positions.append((ped[0], ped[1], 0))

            else:
                next_row, next_column = ped[0], ped[1]
                current_cost = compute_cost_function((next_row, next_column), target_position, obstacles)
                for neighbor in neighbors:
                    neighbor_row, neighbor_column = neighbor[1], neighbor[2]
                    cost = compute_cost_function((neighbor_row, neighbor_column), target_position, obstacles)
                    if cost <= current_cost and grid[neighbor_row, neighbor_column] != 1:
                        current_cost = cost
                        next_row, next_column = neighbor_row, neighbor_column
                grid[ped[0], ped[1]] = 0
                grid[next_row, next_column] = 1
                new_positions.append((next_row, next_column, ped[2] + distance_euclid(ped, [next_row, next_column]) - 1))

        else:  # Leave pedestrian at his position and decrement his move timer
            new_positions.append((ped[0], ped[1], ped[2] - 1))

    return grid, new_positions

def compute_distances_with_dijkstra(grid):
    # Computing the shortest distance to the target for a given cell using the Dijkstra algorithm
    dim = grid.shape
    number_obstacles = 0

    # Initialization of an array giving distance to the target for each cell and of a list with cells without computed
    # distance yet
    distances = np.zeros((dim[0], dim[1]))
    cells_to_evaluate = []

    for i in range(dim[0]):
        for j in range(dim[1]):
            if grid[i][j] != 2 and grid[i][j] != 5:
                distances[i][j] = 9999
                cells_to_evaluate.append((i, j))
            if grid[i][j] == 2:
                distances[i][j] = 0
                cells_to_evaluate.append((i, j))

    suitable_cells = len(cells_to_evaluate)

    for i in range(suitable_cells):  # Assigning a distance to every suitable cell
        min_distance = distances[cells_to_evaluate[0]]
        for c in cells_to_evaluate:
            if distances[c[0], c[1]] <= min_distance:
                min_distance = distances[c[0], c[1]]
                next_cell = c
        neighbors = get_neighbors(grid, next_cell)
        for n in neighbors:
            if (next_cell[0] == n[1] or next_cell[1] == n[2]) : #if the neigbhor is on the same row or column
                if distances[next_cell[0], next_cell[1]] + 1 < distances[n[1], n[2]] :
                    distances[n[1], n[2]] = distances[next_cell[0], next_cell[1]] + 1
            else :
                if distances[next_cell[0], next_cell[1]] + np.sqrt(2) < distances[n[1], n[2]] :
                    distances[n[1], n[2]] = distances[next_cell[0], next_cell[1]] + np.sqrt(2)
        cells_to_evaluate.remove(next_cell)

    return distances

def compute_step_Dijkstra(grid, pedestrians, target_position, distances):
    new_positions = []
    for ped in pedestrians:
        if ped[2] <= 0:  # If pedestrian can move
            neighbors = get_neighbors(grid, ped)
            # If there's a target as neighbor, stop moving
            if 2 in [neighbor[0] for neighbor in neighbors]:
                print('Pedestrian reached the target')
                new_positions.append((ped[0], ped[1], 0))

            else:
                current_distance = distances[ped[0], ped[1]]
                next_row, next_column = ped[0], ped[1]
                for neighbor in neighbors:
                    neighbor_row, neighbor_column = neighbor[1], neighbor[2]
                    dist = distances[neighbor_row, neighbor_column]
                    if dist <= current_distance and grid[neighbor_row, neighbor_column] != 1:
                        current_distance = dist
                        next_row, next_column = neighbor_row, neighbor_column
                grid[ped[0], ped[1]] = 0
                grid[next_row, next_column] = 1
                new_positions.append((next_row, next_column, ped[2] + distance_euclid(ped, [next_row, next_column]) - 1))

        else:  # Leave pedestrian at his position and decrement his move timer
            new_positions.append((ped[0], ped[1], ped[2] - 1))

    return grid, new_positions


def compute_step(grid, pedestrians, target_position):
    new_positions = []
    for ped in pedestrians:
        if ped[2] <= 0:  # If pedestrian can move
            neighbors = get_neighbors(grid, ped)

            # If there's a target as neighbor, stop moving
            if 2 in [neighbor[0] for neighbor in neighbors]:
                print('Pedestrian reached the target')
                new_positions.append((ped[0], ped[1], 0))

            else:
                next_move = find_next_move_euclid(ped, neighbors, target_position)
                # If there is a move, perform it
                if next_move != ped:
                    grid[ped[0], ped[1]] = 0
                    grid[next_move[0], next_move[1]] = 1
                    new_positions.append((next_move[0], next_move[1], ped[2] + next_move[2] - 1))
                else:
                    new_positions.append((next_move[0], next_move[1], 0))

        else:  # Leave pedestrian at his position and decrement his move timer
            new_positions.append((ped[0], ped[1], ped[2] - 1))

    return grid, new_positions


# Create a grid with specified size, add pedestrians, obstacles and the target. Returns that grid.
def initialize_grid(grid_size, pedestrians, obstacles, target_pos):
    grid = define_grid(grid_size[1], grid_size[0])
    for ped in pedestrians:
        grid = add_pedestrian(grid, ped)
    for obs in obstacles:
        grid = add_obstacle(grid, obs)
    grid = add_target(grid, target_pos)
    return grid


# Returns a list of pedestrians, list of obstacles, target position and grid dimensions
def load_scenario(path):
    sec = "default"
    config = configparser.RawConfigParser()
    config.read(path)

    pedestrians = []

    for i in range(int(config.get(sec, "pedestrians"))):
        # Get the pedestrian line and split at the ',' then convert to int and subtract 1 (for proper array coords)
        # convert to tuple and add to list of pedestrians. Pretty involved line.
        temp = tuple([int(val) - 1 for val in config.get(sec, "pedestrian{}".format(i)).split(",")])
        pedestrians.append(temp)

    obstacles = []

    for i in range(int(config.get(sec, "obstacles"))):
        # Analogously for obstacles
        temp = tuple([int(val) - 1 for val in config.get(sec, "obstacle{}".format(i)).split(",")])
        obstacles.append(temp)

    target = tuple([int(val) - 1 for val in config.get(sec, "target").split(",")])

    grid_size = tuple([int(val) for val in config.get(sec, "grid_size").split(",")])

    return pedestrians, target, obstacles, grid_size


# Load variables that control the simulation
def load_simulation_variables(path):
    sec = "default"
    config = configparser.RawConfigParser()
    config.read(path)

    cycles = int(config.get(sec, "cycles"))
    run_forever = "True" == config.get(sec, "run_forever")  # A way to parse string to bool
    simulation_speed = int(config.get(sec, "simulation_speed"))
    return cycles, run_forever, simulation_speed


# Initialize simulation by setting up everything that's necessary
# path: Path to the scenario config file that should be used for the simulation.
def init_simulation(path):
    # Load scenario from file
    pedestrians, target_pos, obstacles, grid_size = load_scenario(path)

    # Initialize grid
    grid = initialize_grid(grid_size, pedestrians, obstacles, target_pos)

    # Add move timer to pedestrian tuple
    pedestrians = [p + (0,) for p in pedestrians]

    # Return needed variables
    return pedestrians, target_pos, grid, obstacles
