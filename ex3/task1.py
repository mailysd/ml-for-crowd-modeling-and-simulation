import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA

w = 1
Y, X = np.mgrid[-w:w:20j, -w:w:20j]


def calc_field_matr1(alpha):
    U = alpha * X + alpha * Y
    V = -0.25 * X
    return U, V


def calc_field_matr2(alpha):
    U = alpha * X
    V = - 0.3 * X + alpha * Y
    return U, V


def calc_field_matr3(alpha):
    U = alpha * X + alpha * Y
    V = 0.25 * X
    return U, V


fig = plt.figure(figsize=[10, 15])

#  Plot Vector Field
plots = []
i = 0
# Matrix 1: Saddle: -0.55 | Unstable: Node: 0.1 Focus: 1
# Matrix 2: Stable: Node: -0.3
# Matrix 3: Focus: -0.25
for n in np.mgrid[-1:1:9j]:
#for n in [-0.25]:
    temp = fig.add_subplot(3, 3, i+1)
    U, V = calc_field_matr1(n)
    temp.streamplot(X, Y, U, V, linewidth=0.5, color="black")
    w, _ = LA.eig(np.array([[n, -0.25], [n, 0]])) # Matrix 1 Eigenvalues
    # w, _ = LA.eig(np.array([[n, -0.3], [0, n]])) # Matrix 2 Eigenvalues
    # w, _ = LA.eig(np.array([[n, 0.25], [n, 0]])) # Matrix 3 Eigenvalues
    temp.set_title('α = {}, λ\u2081 = {}, λ\u2082 = {}'.format(format(n, '.2f'), "{:.2f}".format(w[0]), "{:.2f}".format(w[1])))
    i += 1

plt.show()