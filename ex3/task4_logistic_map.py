# xn+1=rxn(1−xn), n∈N,

import numpy as np
import matplotlib.pyplot as plt
plt.style.use('fast')


def logistic_map(xn, r):
    return r * xn * (1 - xn)


# Function used to plot the evolution for xn with the logistic map
def logistic_map_evolution_plot(x_start, r):
    values = []
    fig = plt.figure()
    plot = fig.add_subplot()
    plot.set_title("x = {}, r = {}".format('%.3f' % x_start, '%.3f' % r))
    plot.set_xlabel("n")
    plot.set_ylabel("x\u2099")

    # Plot evolution of xn
    for _ in range(100):
        values.append(x_start)
        x_start = logistic_map(x, r)

    plot.plot(values)
    plt.show()
    print(values)


# Start at value 0 for r and increase it to 4 in n steps
n = 50000
r = np.linspace(0.0, 4.0, n)

# Do 1000 iterations of the logistic map and use the last 100 values to plot the bifurcation diagram
it = 1000
last = 100

# Initialize the system with the same starting value 0.5
x = 0.5 * np.ones(n)

fig, ax = plt.subplots(1, 1)

# Plot logistic map bifurcation diagram
for i in range(it):
    x = logistic_map(x, r)
    # We display the bifurcation diagram.
    if i >= (it - last):
        ax.plot(r, x, ',k', antialiased=True, alpha=0.05)

ax.set_xlim(0, 4)
ax.set_xlabel("r")
ax.set_ylabel("x")
ax.set_title("Bifurcation diagram")

plt.show()
