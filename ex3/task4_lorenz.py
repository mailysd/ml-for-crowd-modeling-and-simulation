# Disclaimer: The basis for this code has been taken from
# https://matplotlib.org/3.3.2/gallery/mplot3d/lorenz_attractor.html
import matplotlib.pyplot as plt
import numpy as np
import numpy.linalg as linalg


def lorenz(x, y, z, s=10, r=28, b=(8. / 3.)):
    """
    Given:
       x, y, z: a point of interest in three dimensional space
       s, r, b: parameters defining the lorenz attractor
    Returns:
       x_dot, y_dot, z_dot: values of the lorenz attractor's partial
           derivatives at the point x, y, z
    """
    x_dot = s * (y - x)
    y_dot = r * x - y - x * z
    z_dot = x * y - b * z
    return x_dot, y_dot, z_dot


# Using 100000 steps with a dt of 0.01 leaves us at T = steps * dt = 1000
dt = 0.01
num_steps = 100000

# Need one more for the initial values
xs = np.empty(num_steps + 1)
ys = np.empty(num_steps + 1)
zs = np.empty(num_steps + 1)

# Set initial values
xs[0], ys[0], zs[0] = (10., 10., 10.)

# Step through "time", calculating the partial derivatives at the current point
# and using them to estimate the next point
for i in range(num_steps):
    x_dot, y_dot, z_dot = lorenz(xs[i], ys[i], zs[i])
    xs[i + 1] = xs[i] + (x_dot * dt)
    ys[i + 1] = ys[i] + (y_dot * dt)
    zs[i + 1] = zs[i] + (z_dot * dt)

# Do the same for very similar starting conditions
# Need one more for the initial values
xs2 = np.empty(num_steps + 1)
ys2 = np.empty(num_steps + 1)
zs2 = np.empty(num_steps + 1)

# Set initial values
xs2[0], ys2[0], zs2[0] = (10. + 1e-8, 10., 10.)

# Step through "time", calculating the partial derivatives at the current point
# and using them to estimate the next point
for i in range(num_steps):
    x_dot, y_dot, z_dot = lorenz(xs2[i], ys2[i], zs2[i])
    xs2[i + 1] = xs2[i] + (x_dot * dt)
    ys2[i + 1] = ys2[i] + (y_dot * dt)
    zs2[i + 1] = zs2[i] + (z_dot * dt)

# Now check when the distance between the two plots is bigger than 1
for i in range(num_steps):
    # Use euclidean norm to find distance between two points
    a = np.array([xs[i], ys[i], zs[i]])
    b = np.array([xs2[i], ys2[i], zs2[i]])
    distance = linalg.norm(np.array(a - b))
    if distance >= 1:
        print(
            "The distance between the two systems is {} at timestep {} with the values of the systems being {} and {}.".format(distance, i, a, b))
        break

# Plot
fig = plt.figure()
ax = fig.gca(projection='3d')

ax.plot(xs, ys, zs, lw=0.1)
ax.plot(xs2, ys2, zs2, lw=0.1, color="sandybrown")
ax.set_xlabel("X Axis")
ax.set_ylabel("Y Axis")
ax.set_zlabel("Z Axis")
ax.set_title("Lorenz Attractors for x\u2080=({}, {}, {}) in blue and x\u2080=({}, {}, {}) in orange".format(xs[0], ys[0], zs[0], xs2[0], ys2[0], zs2[0]))

plt.show()
