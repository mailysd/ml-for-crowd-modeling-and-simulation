import numpy as np
import matplotlib.pyplot as plt
import scipy.misc

class PCA_custom() :
    def __init__(self,n_components) :
        self.explained_variance_ratio_ = 0
        self.variance_ = 1
        if n_components >= 1 and isinstance(n_components, int) :
            self.n_components_ = n_components
        else :
            self.n_components_ = 0 #the number of component is not fixed yet
            self.variance_ratio = n_components
        
        
    def fit(self, dataset) :
        X = np.array(dataset)
        self.n_samples, self.n_features = X.shape
        self.mean_ = np.mean(X, axis=0)
        center_X = X - self.mean_
        
        U, S, self.V = np.linalg.svd(center_X)
        denom = np.sum(S**2)
        self.explained_variance_ratio_ = [S[i]**2/denom for i in range(self.n_components_)]
        self.U, self.S = U[:, :self.n_components_],S[:self.n_components_]
    
    def transform(self, X) :
        self.mean_ = np.mean(X, axis=0)
        center_X = X - self.mean_
        X_transform = center_X@self.V.T
        if self.n_components_ > 0 :
            X_transform = (X_transform.T[:self.n_components_]).T
        else : 
            #if the variance_ratio is given isntead of the number of components, the number of components to keep is found
            #by taking the smallest number of components  needed to obtain the desired variance_ratio
            comp = 0
            while np.sum(self.explained_variance_ratio) < self.variance_ratio :
                comp += 1
            self.n_components = comp
            X_transform = (X_transform.T[:self.n_components_]).T
        self.explained_variance_ratio_ = self.explained_variance_ratio_[:self.n_components_]
        return X_transform
        
    def fit_transform(self, dataset) :
        self.fit(dataset)
        return self.transform(dataset)
        
    def inverse_transform(self, decomposition) :
        decomposition = np.array(decomposition)
        n_samples = decomposition.shape[0]
        new_set = np.zeros((n_samples,self.n_features))
        for i in range(n_samples) :
            for j in range(self.n_components_) :
                new_set[i][j] = decomposition[i][j]
        return (new_set@(self.V) + self.mean)
        