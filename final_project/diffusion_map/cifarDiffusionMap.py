import diffusionMap
import time
from utils.retrieveDataCifar import get_CIFAR10_data, reformat_dataset

# Extracting dataset #
x_train, y_train, X_val, y_val, x_test, y_test = get_CIFAR10_data()
x_train = reformat_dataset(x_train)

# Choose number on samples to compute diffusion map on #
nb_samples = 1000

# Initialize diffusion map #
DM = diffusionMap.DiffusionMap(x_train[:nb_samples].reshape(nb_samples, -1), n_evecs=100, alpha=1.0)

# Create diffusion map embedding #
t0 = time.time()
dmap, eigen_functions, eigen_values = DM.fit_transform()
print('Computation time of diffusion map: {}s \n'.format(time.time()-t0))

print(dmap.shape)
print(eigen_functions.shape)
print(dmap[0])
print(eigen_functions[0])

