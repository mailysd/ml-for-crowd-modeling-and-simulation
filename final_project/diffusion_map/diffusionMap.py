import numpy as np
import scipy.sparse as sps
from scipy.spatial import cKDTree
import scipy.sparse.linalg as spsl
from scipy import sparse
from sklearn.metrics.pairwise import euclidean_distances
from utils.diffusionMapUtils import coo_tocsr, make_stochastic_matrix
import time


class DiffusionMap(object):
    """
    DIffusion map class that takes as input the following parameters:
    - dataset : np.array
        Data from which you want to create the embedding
    - alpha : scalar, optional
        Exponent to be used for the left normalization in constructing the diffusion map.
    - n_evecs : int, optional
        Number of diffusion map eigenvectors to return
    """

    def __init__(self, data, alpha=0.05, n_evecs=1):
        self.data = data
        self.alpha = alpha
        self.n_evecs = n_evecs

    def _compute_kernel_matrix(self):
        ''' Computes the kernel matrix with Sklearn euclidean distance and the parameter alpha '''
        print('--- Computation of euclidean distance ---')
        t0 = time.time()
        distance_matrix = euclidean_distances(self.data, self.data)
        t1 = time.time() - t0
        print('Computation time of Euclidean distance: {}s \n'.format(t1))

        self.eta = self.alpha * np.amax(distance_matrix)
        print('--- Computation of kernel matrix ---')
        t0 = time.time()
        kernel_matrix = np.exp(-distance_matrix ** 2 / self.eta)
        t1 = time.time() - t0
        print('Computation time of kernel matrix: {}s \n'.format(t1))

        return kernel_matrix


    def _diagonal_normalization(self, matrix, exhibitor):
        ''' Computes the sparse normalized matrix of the parameter matrix'''
        diag = matrix.sum(axis=1).transpose()
        normalized_matrix = sps.diags(np.power(diag, exhibitor), 0, shape=(diag.shape[0], diag.shape[0]))
        return normalized_matrix


    def _find_eigen_values_and_vectors(self, T):
        ''' Computes eigen vectors and eigen values of the embedding'''
        evals, evecs = spsl.eigs(T, k=(self.n_evecs + 1), which='LM')
        ix = evals.argsort()[::-1][1:]
        evals = np.real(evals[ix])
        evecs = np.real(evecs[:, ix])
        dmap = np.dot(evecs, np.diag(evals))
        return dmap, evecs, evals


    def fit_transform(self):
        """ Returns the diffusion map coordinates of the dataset"""
        kernel_matrix = self._compute_kernel_matrix()

        print('--- Normalizing kernel matrices ---')
        t0 = time.time()
        print('- Computation of P -')
        P_inv = self._diagonal_normalization(kernel_matrix, -1)
        print('- Computation of K -')
        K = sps.csr_matrix.dot(P_inv, sps.csr_matrix.dot(kernel_matrix, P_inv))
        print('- Computation of Q -')
        Q_inv2 = (self._diagonal_normalization(K, -1)).power(1/2)  # A modifier to.array()
        print('- Computation of T -')
        T = sps.csr_matrix.dot(Q_inv2, sps.csr_matrix.dot(K, Q_inv2))

        t1 = time.time() - t0
        print('Computation time of normalization: {}s \n'.format(t1))

        print('--- Finding eigen values ---')
        t0 = time.time()
        dmap, eigen_vectors, eigen_values = self._find_eigen_values_and_vectors(T)
        t1 = time.time() - t0
        print('Computation time of calculating eigen values & vectors: {}s \n'.format(t1))

        return dmap, eigen_vectors, eigen_values

#############################################################
# Functions to improve the computation of the kernel matrix #
#############################################################

    def _compute_kernel_matrix_sparse(self):
        print('--- Computation of sparse distance matrix ---')
        self.kdtree = cKDTree(self.data)
        print('a')
        distance_matrix = self.kdtree.sparse_distance_matrix(self.kdtree, 1, output_type='coo_matrix')
        distance_matrix = coo_tocsr(distance_matrix)  # A rajouter utils
        kernel_matrix = self.compute_intermediate_kernel_matrix(distance_matrix)
        kernel_matrix = self.normalize_kernel_matrix(kernel_matrix)

        print(kernel_matrix)
        print('b')
        return kernel_matrix


    def normalize_kernel_matrix(self, matrix):
        shape = matrix.shape

        row_sums = np.asarray(matrix.sum(axis=1)).squeeze()

        inv_diag = 1.0 / row_sums ** self.alpha
        inv_diag[np.isnan(inv_diag)] = 0.0
        Dinv = sparse.spdiags(inv_diag, 0, shape[0], shape[1])

        Wtilde = Dinv @ matrix @ Dinv

        return make_stochastic_matrix(Wtilde)  # A rajouter utils


    def compute_intermediate_kernel_matrix(self, distance_matrix):
        """Compute kernel matrix.
        Returns the (unnormalized) Gaussian kernel matrix corresponding to
        the data set and choice of bandwidth `epsilon`.
        Parameters
        ----------
        distance_matrix : scipy.sparse.spmatrix
            A sparse matrix whose entries are the distances between data
            points.
        Returns
        -------
        kernel_matrix : scipy.sparse.spmatrix
            A similarity matrix (unnormalized kernel matrix) obtained by
            applying `kernel_function` to the entries in `distance_matrix`.
        """
        data = distance_matrix.data
        self.eta = np.amax(data)
        transformed_data = np.exp(-np.square(data) / (2.0 * self.eta))
        kernel_matrix = distance_matrix._with_data(transformed_data, copy=True)
        return kernel_matrix

