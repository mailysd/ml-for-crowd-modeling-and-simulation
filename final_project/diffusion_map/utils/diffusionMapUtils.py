#################
# Diffusion Map #
#################

import numpy as np
from scipy import sparse

def coo_tocsr(A):
    """Convert matrix to Compressed Sparse Row format, fast.
    This function is derived from the corresponding SciPy code but it avoids
    the sanity checks that slow `scipy.sparse.coo_matrix.to_csr down`. In
    particular, by not summing duplicates we can attain important speed-ups
    for large matrices.
    """
    from scipy.sparse import csr_matrix
    if A.nnz == 0:
        return csr_matrix(A.shape, dtype=A.dtype)

    m, n = A.shape
    # Using 32-bit integer indices allows for matrices of up to 2,147,483,647
    # non-zero entries.
    idx_dtype = np.int32
    row = A.row.astype(idx_dtype, copy=False)
    col = A.col.astype(idx_dtype, copy=False)

    indptr = np.empty(n+1, dtype=idx_dtype)
    indices = np.empty_like(row, dtype=idx_dtype)
    data = np.empty_like(A.data, dtype=A.dtype)

    sparse._sparsetools.coo_tocsr(m, n, A.nnz, row, col, A.data,
                                        indptr, indices, data)

    return csr_matrix((data, indices, indptr), shape=A.shape)



def make_stochastic_matrix(matrix):
    """Convert a sparse non-negative matrix into a row-stochastic matrix.
    Carries out the normalization (in the 1-norm) of each row of a
    non-negative matrix inplace.  The matrix should be in Compressed
    Sparse Row format. Note that this method overwrites the input matrix.
    Parameters
    ----------
    matrix : scipy.sparse.csr_matrix
        A matrix with non-negative entries to be converted.
    Returns
    -------
    matrix : scipy.sparse.csr_matrix
        The same matrix passed as input but normalized.
    """
    data = matrix.data
    indptr = matrix.indptr
    for i in range(matrix.shape[0]):
        a, b = indptr[i:i+2]
        norm1 = np.sum(data[a:b])
        data[a:b] /= norm1
    return matrix

